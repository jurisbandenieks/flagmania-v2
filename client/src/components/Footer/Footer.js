import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer__container">Juris Bandenieks &copy;2020</div>
    </div>
  );
};

export default Footer;
