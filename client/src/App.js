import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.scss";
import { Provider } from "react-redux";
import store from "./store";

import Footer from "./components/Footer/Footer";
import Home from "./pages/Home/Home";
import Game from "./pages/Game/Game";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="app">
          <main className="main">
            <Switch>
              <Route exact path="/" component={Home}></Route>
              <Route exact path="/game" component={Game}></Route>
            </Switch>
          </main>
          <Footer />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
