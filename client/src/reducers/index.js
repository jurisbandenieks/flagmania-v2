import { combineReducers } from "redux";
import flagsReducer from "./flagsReducer";
import userReducer from "./userReducer";

export default combineReducers({ users: userReducer, flags: flagsReducer });
