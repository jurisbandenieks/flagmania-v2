import {
  SET_LEVEL,
  ADD_STREAK,
  RESTART_STREAK,
  GET_USERS,
  SET_USER,
  SET_SCORE,
  CLEAR_SCORE
} from "../actions/types";

const initialState = {
  user: { username: "", score: 0 },
  levels: [
    { text: "easy", value: 1 },
    { text: "medium", value: 2 },
    { text: "hard", value: 3 }
  ],
  level: { text: "", value: null },
  users: [],
  streak: 1
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LEVEL:
      return {
        ...state,
        level: action.payload
      };
    case ADD_STREAK:
      return {
        ...state,
        streak: state.streak + 1
      };
    case RESTART_STREAK:
      return {
        ...state,
        streak: initialState.streak
      };
    case GET_USERS:
      return {
        ...state,
        users: action.payload
      };
    case SET_USER:
      return {
        ...state,
        users: action.payload
      };
    case SET_SCORE:
      return {
        ...state,
        user: { score: state.user.score + action.payload }
      };
    case CLEAR_SCORE:
      return {
        ...state,
        user: { score: 0 }
      };
    default:
      return state;
  }
};
