import {
  SET_LEVEL,
  ADD_STREAK,
  RESTART_STREAK,
  GET_USERS,
  SET_USER,
  SET_SCORE,
  CLEAR_SCORE
} from "./types";
import axios from "axios";

export const setLevel = (level) => (dispatch) => {
  dispatch({
    type: SET_LEVEL,
    payload: level
  });
};

export const getUsers = () => async (dispatch) => {
  const url = "https://flagmania.herokuapp.com/api/v1/users";

  const users = await axios.get(url);

  dispatch({
    type: GET_USERS,
    payload: users.data.data
  });
};

export const addUser = (user) => async (dispatch) => {
  const addUrl = "https://flagmania.herokuapp.com/api/v1/user";

  await axios.post(addUrl, user);

  const getUrl = "https://flagmania.herokuapp.com/api/v1/users";

  const users = await axios.get(getUrl);

  dispatch({
    type: SET_USER,
    payload: users.data.data
  });
};

export const setScore = (score) => (dispatch) => {
  dispatch({
    type: SET_SCORE,
    payload: score
  });
};

export const addStreak = () => {
  return {
    type: ADD_STREAK
  };
};

export const clearStreak = () => {
  return {
    type: RESTART_STREAK
  };
};

export const clearScore = () => {
  return {
    type: CLEAR_SCORE
  };
};
