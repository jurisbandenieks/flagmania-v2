export const GET_COUNTRIES = "GET_COUNTRIES";

export const SET_LEVEL = "SET_LEVEL";
export const GET_USERS = "GET_USERS";
export const SET_USER = "SET_USER";
export const SET_SCORE = "SET_SCORE";
export const CLEAR_SCORE = "CLEAR_SCORE";
export const ADD_STREAK = "ADD_STREAK";
export const RESTART_STREAK = "RESTART_STREAK";
