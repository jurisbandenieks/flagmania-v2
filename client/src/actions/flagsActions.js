import { GET_COUNTRIES } from "./types";

export const getCountries = (level) => (dispatch) => {
  const countries = {
    easy: [
      "argentina",
      "australia",
      "austria",
      "bahamas",
      "belarus",
      "belgium",
      "bolivia",
      "brazil",
      "bulgaria",
      "canada",
      "chile",
      "china",
      "cuba",
      "czech republic",
      "denmark",
      "finland",
      "france",
      "germany",
      "greece",
      "hungary",
      "iceland",
      "india",
      "ireland",
      "italy",
      "japan",
      "malaysia",
      "mexico",
      "netherlands",
      "new zealand",
      "north korea",
      "norway",
      "panama",
      "peru",
      "philippines",
      "poland",
      "portugal",
      "romania",
      "russia",
      "saudi arabia",
      "serbia",
      "singapore",
      "slovakia",
      "slovenia",
      "south africa",
      "south korea",
      "spain",
      "sweden",
      "switzerland",
      "turkey",
      "uganda",
      "ukraine",
      "united kingdom",
      "united states of america",
      "vatican city",
      "venezuela"
    ],
    medium: [
      "afghanistan",
      "albania",
      "algeria",
      "andorra",
      "armenia",
      "azerbaijan",
      "bahrain",
      "bangladesh",
      "barbados",
      "bosnia and herzegovina",
      "botswana",
      "cambodia",
      "cameroon",
      "central african republic",
      "colombia",
      "congo democratic republic",
      "congo republic",
      "costa rica",
      "cote d ivoire",
      "croatia",
      "cyprus",
      "dominica",
      "dominican republic",
      "ecuador",
      "egypt",
      "estonia",
      "ethiopia",
      "georgia",
      "haiti",
      "indonesia",
      "iran",
      "israel",
      "jamaica",
      "kazakhstan",
      "kenya",
      "kosovo",
      "kuwait",
      "kyrgyzstan",
      "laos",
      "latvia",
      "lebanon",
      "libya",
      "liechtenstein",
      "lithuania",
      "luxembourg",
      "macedonia",
      "malta",
      "mauritania",
      "monaco",
      "mongolia",
      "montenegro",
      "morocco",
      "myanmar",
      "namibia",
      "nepal",
      "niger",
      "nigeria",
      "oman",
      "pakistan",
      "paraguay",
      "qatar",
      "rwanda",
      "san marino",
      "sri lanka",
      "sudan",
      "syria",
      "taiwan",
      "tajikistan",
      "tanzania",
      "thailand",
      "trinidad and tobago",
      "tunisia",
      "turkmenistan",
      "united arab emirates",
      "uruguay",
      "uzbekistan",
      "vietnam",
      "zambia",
      "zimbabwe"
    ],
    hard: [
      "angola",
      "antigua and barbuda",
      "belize",
      "benin",
      "bhutan",
      "brunei",
      "burkina faso",
      "burundi",
      "cape verde",
      "chad",
      "comoros",
      "djibouti",
      "east timor",
      "el salvador",
      "equatorial guinea",
      "eritrea",
      "fiji",
      "gabon",
      "gambia",
      "ghana",
      "grenada",
      "guatemala",
      "guinea bissau",
      "guinea",
      "guyana",
      "honduras",
      "kiribati",
      "lesotho",
      "liberia",
      "madagascar",
      "malawi",
      "maldives",
      "mali",
      "marshall islands",
      "mauritius",
      "micronesia",
      "moldova",
      "mozambique",
      "nauru",
      "nicaragua",
      "niue",
      "palau",
      "papua new guinea",
      "saint kitts and nevis",
      "saint lucia",
      "saint vincent and the grenadines",
      "samoa",
      "sao tome and principe",
      "senegal",
      "seychelles",
      "sierra leone",
      "solomon islands",
      "somalia",
      "south sudan",
      "suriname",
      "swaziland",
      "togo",
      "tonga",
      "tuvalu",
      "yemen"
    ]
  };

  let countriesToGuess = ["argentina", "belgium", "canada", "germany"];
  let country = "belgium";
  if (level) {
    let randomFourDigits = [];

    for (let i = 0; i < 4; i++) {
      const r =
        Math.floor(Math.random() * (countries[level].length - 1 - 0 + 1)) + 0;
      const isAlreadyRandom = randomFourDigits.find((d) => d === r);
      if (!isAlreadyRandom) {
        randomFourDigits.push(r);
      } else {
        i--;
      }
    }

    const randomOneDigit =
      Math.floor(Math.random() * (randomFourDigits.length - 1 - 0 + 1)) + 0;

    countriesToGuess = countriesToGuess.map(
      (country, index) => countries[level][randomFourDigits[index]]
    );

    country = countriesToGuess[randomOneDigit];
  }
  dispatch({
    type: GET_COUNTRIES,
    payload: { countriesToGuess, country }
  });
};

export const clearCountries = () => {
  return {
    type: GET_COUNTRIES,
    payload: { countriesToGuess: [], country: "" }
  };
};
