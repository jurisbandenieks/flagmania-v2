import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { setLevel } from "../../actions/userActions";
import { clearCountries } from "../../actions/flagsActions";

import "./Home.scss";

const Home = ({
  userData: { levels },
  flagsData: { countries },
  setLevel,
  clearCountries
}) => {
  useEffect(() => {
    setLevel("");
    clearCountries();
    cacheImages();
    // eslint-disable-next-line
  }, []);

  const cacheImages = () => {
    if (countries) {
      if (countries.length === levels.length) {
        for (let i = 0; i < countries.length; i++) {
          const level = levels[i].text;
          const element = countries[i];
          for (let j = 0; j < element.length; j++) {
            let img = new Image();
            const country = element[j];
            img.src = require(`../../images/flags/${level}/${country}.png`);
          }
        }
      }
    }
  };

  return (
    <div className="home">
      {levels &&
        levels.map((level, index) => (
          <Link
            to="/game"
            className={"home__level home__level--" + level.text}
            key={index}
            onClick={() => setLevel(level)}
          >
            <h1>{level.text.toUpperCase()}</h1>
          </Link>
        ))}
    </div>
  );
};

const mapStateToProps = (state) => ({
  userData: state.users,
  flagsData: state.flags
});

export default connect(mapStateToProps, { setLevel, clearCountries })(Home);
