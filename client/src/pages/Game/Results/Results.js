import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { getUsers, addUser } from "../../../actions/userActions";

import "./Results.scss";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

const Results = ({ show, usersData: { user, users }, getUsers, addUser }) => {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    getUsers();
    // eslint-disable-next-line
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    if (username) {
      addUser({ username: username, score: user.score });
      setUsername("");
      setDisabled(true);
    }
  };

  return (
    <Modal show={show} backdrop="static" keyboard={false}>
      <ModalHeader>
        <ModalTitle className="results-body__header">
          Score: {user.score}
        </ModalTitle>
      </ModalHeader>
      <ModalBody className="results-body">
        <Form
          action="submit"
          onSubmit={(e) => onSubmit(e)}
          className="results-body__input"
        >
          <Form.Group>
            <Form.Control
              type="text"
              name="name"
              placeholder="Enter name"
              size="sm"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>
          <Button
            variant="primary"
            type="submit"
            disabled={disabled || !username}
          >
            Submit
          </Button>
        </Form>

        <h4>Top 10</h4>
        <table>
          <thead>
            <tr>
              <th>Place</th>
              <th>Name</th>
              <th>Score</th>
            </tr>
          </thead>

          <tbody>
            {users.length > 0 &&
              users.map((user, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{user.username}</td>
                  <td>{user.score}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        <Button variant="secondary" onClick={() => history.push("/")}>
          Main Menu
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  usersData: state.users
});

export default connect(mapStateToProps, { getUsers, addUser })(Results);
