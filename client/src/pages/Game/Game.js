import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getCountries } from "../../actions/flagsActions";
import { clearStreak, clearScore } from "../../actions/userActions";

import Flag from "./Flag/Flag";
import Results from "./Results/Results";

import "./Game.scss";

const Game = ({
  flagsData: { countriesToGuess, country },
  usersData: { user, level },
  getCountries,
  clearStreak,
  clearScore
}) => {
  const [count, setCount] = useState(100);
  const [show, setShow] = useState(false);
  const [counter, setCounter] = useState(3);
  const history = useHistory();

  const flagsBlock = document.getElementById("flags");

  useEffect(() => {
    if (level.text === "") {
      history.push("/");
    }

    setCount(100);
    clearStreak();
    clearScore();
    getCountries(level.text);

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    let countDown = setInterval(() => {
      if (counter > 0) {
        setCounter(counter - 1);
      }
      if (counter <= 0) {
        clearInterval(countDown);
      }
    }, 1000);
    return () => {
      clearInterval(countDown);
    };
    // eslint-disable-next-line
  }, [counter]);

  useEffect(() => {
    if (counter <= 0) {
      let timer = setInterval(() => {
        if (count >= 0) {
          setCount(count - 1);
        } else {
          setShow(true);
          clearInterval(timer);
        }
      }, 300);

      return () => {
        clearInterval(timer);
      };
    }
    // eslint-disable-next-line
  }, [counter, count]);

  const transition = (show) => {
    if (show) {
      flagsBlock.style.display = "grid";
    } else {
      flagsBlock.style.display = "none";
    }
  };

  return counter <= 0 ? (
    <div className="game">
      <div className="game__headline">
        <h1 className="game__header">{country.toUpperCase()}</h1>
        <div className="game__timer">
          <div className="game__time-left" style={{ width: `${count}%` }}></div>
        </div>
      </div>
      <div className="game__flags">
        <div id="flags">
          {countriesToGuess &&
            countriesToGuess.length > 0 &&
            countriesToGuess.map((item, index) => {
              return (
                <Flag
                  key={index}
                  flag={item}
                  time={count}
                  transition={transition}
                />
              );
            })}
        </div>
      </div>
      <div className="game__actions">
        <Link to="/" className="btn">
          Main Menu
        </Link>
        <div className="game__points">{user.score}</div>
      </div>

      <Results className="results" show={show} />
    </div>
  ) : (
    <div className="count-down">
      <h1>{counter}</h1>
    </div>
  );
};

const mapStateToProps = (state) => ({
  flagsData: state.flags,
  usersData: state.users
});

export default connect(mapStateToProps, {
  getCountries,
  clearStreak,
  clearScore
})(Game);
