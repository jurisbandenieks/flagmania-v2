import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getCountries } from "../../../actions/flagsActions";
import { addStreak, clearStreak, setScore } from "../../../actions/userActions";

import "./Flag.scss";

const Flag = ({
  flag,
  time,
  usersData: { level, streak },
  flagsData: { country },

  transition,
  addStreak,
  clearStreak,
  getCountries,
  setScore
}) => {
  const [opacity, setOpacity] = useState(1);

  useEffect(() => {
    setOpacity(1);
  }, [country]);

  const onClick = (flagName) => {
    if (flagName.toLowerCase() === country.toLowerCase() && time > 0) {
      transition(false);
      getCountries(level.text);

      setTimeout(() => {
        transition(true);

        setScore(10 * streak * level.value);

        addStreak();
      }, 300);
    } else {
      clearStreak();
      setOpacity(0.5);
    }
  };

  return (
    <div className="flag">
      <img
        src={require(`../../../images/flags/${level.text}/${flag}.png`)}
        style={{ opacity: opacity }}
        alt="flag"
        onClick={(e) => onClick(flag)}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  flagsData: state.flags,
  usersData: state.users
});

export default connect(mapStateToProps, {
  getCountries,
  addStreak,
  clearStreak,
  setScore
})(Flag);
