const User = require("../models/users");

exports.getUsers = async (req, res, next) => {
  try {
    const users = await User.find().sort({ score: -1 }).limit(10);

    console.log("RECEIVED TOP 10 USERS: ", users);

    res.status(200).json({ success: true, data: users });
  } catch (error) {
    res.status(400).json({ success: false, msg: error.message });
  }
};

exports.addUser = async (req, res, next) => {
  try {
    const user = await User.create(req.body);

    console.log("ADDING USER: ", user);

    res.status(201).json({ success: true, data: user });
  } catch (error) {
    res.status(400).json({ success: false, msg: error.message });
  }
};
