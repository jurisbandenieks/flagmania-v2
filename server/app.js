const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const helmet = require("helmet");
const connectDB = require("./config/db");

dotenv.config({ path: "./config/config.env" });

// Connect to database
connectDB();

// Route files
const users = require("./routes/users");

const PORT = process.env.PORT || 9000;

var corsOptions = {
  origin: "https://flagmania-jurisbandenieks.netlify.app",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const cors = require("cors");
const app = express();

app.use(helmet());
app.use(cors(corsOptions));

if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use(express.json());

// Mount routers
app.use("/api/v1", users);

const server = app.listen(PORT, () =>
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on localhost:${PORT}`
  )
);

// Handle rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);
  // Close server
  server.close(() => process.exit(1));
});
