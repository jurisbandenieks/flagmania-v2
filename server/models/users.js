const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "Please add a name"],
    unique: false,
    trim: true,
    maxlength: [20, "Name cannot be longer than 20 characters"]
  },
  score: {
    type: Number,
    required: [true, "Needs score"]
  }
});

module.exports = mongoose.model("User", UserSchema);
