const express = require("express");
const { getUsers, addUser } = require("../controllers/users");
const router = express.Router();

router.route("/users").get(getUsers);
router.route("/user").post(addUser);

module.exports = router;
